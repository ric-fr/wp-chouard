<?php

// Disable direct access
defined('ABSPATH') or die('No script kiddies please!');

add_action('init', 'register_theia_sticky_sidebar');
add_action('wp_enqueue_scripts', 'enqueue_theia_sticky_sidebar');
add_action('wp_head', 'head_theia_sticky_sidebar');

function register_theia_sticky_sidebar()
{
	$dist = 'node_modules/theia-sticky-sidebar/dist/';
	wp_register_script('resize-sensor', plugins_url($dist . 'ResizeSensor.min.js', __FILE__), ['jquery'], '1');
	wp_register_script('theia-sticky-sidebar', plugins_url($dist . 'theia-sticky-sidebar.min.js', __FILE__), ['resize-sensor'], '1.7.0');
}

function enqueue_theia_sticky_sidebar()
{
	wp_enqueue_script('resize-sensor');
	wp_enqueue_script('theia-sticky-sidebar');
}

function head_theia_sticky_sidebar()
{
?>
	<script>
		(function($) {
			$(function() {

				$('.sticky.sidebar').theiaStickySidebar({

					// container element
					'containerSelector': '#main-content',

					// top/bottom margin in pixels
					'additionalMarginTop': 110,
					'additionalMarginBottom': 0,

					// auto update height on window resize
					'updateSidebarHeight': true,

					// disable the plugin when the screen size is smaller than...
					'minWidth': 0,

					// disable the plugin on responsive layouts
					'disableOnResponsive': true,

					// or 'stick-to-top', 'stick-to-bottom'
					'sidebarBehavior': 'modern',

					// or 'absolute'
					'defaultPosition': 'relative',

					// namespace
					'namespace': 'TSS'

				});

			})
		})(jQuery)
	</script>
<?php
}
