<?php


// add_term_meta( $term_id, $meta_key, $meta_value, $unique );

add_shortcode('list-auteur', 'list_auteur_shortcode');

function list_auteur_shortcode($atts, $content, $tag)
{
	$return = [];

	$post_type = 'auteur';

	$query = [
		// 'post_type' => ['auteur', 'livre', 'citation'],
		'post_type' => $post_type,
		'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'],
		'posts_per_page' => -1
	];

	$posts = get_posts($query);

	// $return[] = '<pre>';

	foreach ($posts as $post) {
		$tax = [];
		$meta = get_post_meta($post->ID, '', true);


		$return[] = esc_html(print_r($meta, true));

		$tax['title'] = $post->post_title;
		$tax['slug'] = $post->post_name;

		// meta
		$tax['bio_auteur'] = ($meta['bio_auteur'][0] != '') ? 'HAS_BIO' : '';
		$tax['annees_auteur'] = $meta['annees_auteur'][0];
		$tax['origine_auteur'] = $meta['origine_auteur'][0];
		$tax['analyses_auteur'] = $meta['analyses_auteur'][0];

		if ($meta['ses_livres']) {
			if (strpos($meta['ses_livres'][0], ':')) {
				$a = unserialize($meta['ses_livres'][0]);
			} else {
				$a = $meta['ses_livres'];
			}

			$livres = is_array($a) ? $a : implode(',', $a);
			$livres = array_filter($livres, fn ($value) => !is_null($value) && $value !== '');
		}

		if ($meta['ses_citations']) {
			if (strpos($meta['ses_citations'][0], ':')) {
				$a = unserialize($meta['ses_citations'][0]);
			} else {
				$a = $meta['ses_citations'];
			}

			$citations = is_array($a) ? $a : implode(',', $a);
			$citations = array_filter($citations, fn ($value) => !is_null($value) && $value !== '');
		}

		if ($meta['analyses_auteur']) {
			if (strpos($meta['analyses_auteur'][0], ':')) {
				$a = unserialize($meta['analyses_auteur'][0]);
			} else {
				$a = $meta['analyses_auteur'];
			}

			$analyses = is_array($a) ? $a : implode(',', $a);
			$analyses = array_filter($analyses, fn ($value) => !is_null($value) && $value !== '');
		}

		// $return[] = esc_html(print_r($tax, true));

		if ($livres && false) {
			foreach ($livres as $livre) {
				$sql = [];
				$sql[] = "rel_key = 'rel_auteur'";
				$sql[] = "from_type = 'post'";
				$sql[] = "from_name = 'auteur'";
				$sql[] = "from_status = 'publish'";
				$sql[] = "from_lang = 'fr_FR'";
				$sql[] = "to_type = 'post'";
				$sql[] = "to_status = 'publish'";
				$sql[] = "to_lang = 'fr_FR'";
				$sql[] = "from_id = " . $post->ID;

				$sql[] = "to_name = 'livre'";
				$sql[] = "to_id = " . $livre;

				$return[] = 'INSERT INTO wp_p2p_relationships SET ' . implode(", ", $sql) . ' ON DUPLICATE KEY UPDATE ' . implode(", ", $sql) . ';';
			}
		}

		if ($citations) {
			$return[] = esc_html(print_r($citations, true));
			foreach ($citations as $citation) {
				$sql = [];
				$sql[] = "rel_key = 'rel_auteur'";
				$sql[] = "from_type = 'post'";
				$sql[] = "from_name = 'rel_auteur'";
				$sql[] = "from_status = 'publish'";
				$sql[] = "from_lang = 'fr_FR'";
				$sql[] = "to_type = 'post'";
				$sql[] = "to_status = 'publish'";
				$sql[] = "to_lang = 'fr_FR'";
				$sql[] = "from_id = " . $post->ID;

				$sql[] = "to_id = " . $citation;
				$sql[] = "to_name = 'citation'";

				$return[] = 'INSERT INTO wp_p2p_relationships SET ' . implode(", ", $sql) . ' ON DUPLICATE KEY UPDATE ' . implode(", ", $sql) . ';';
			}
		}
	}

	// 		$return[] = $post->ID . ' ' . $post->post_name;
	// 		$return[] = $meta['bio_auteur'][0];
	// 		$return[] = $meta['annees_auteur'][0];
	// 		$return[] = $meta['origine_auteur'][0];
	// 		$return[] = $meta['analyses_auteur'][0];
	// 		$return[] = $meta['ses_livres'][0];
	// 		$return[] = $meta['ses_citations'][0];

	// 		$return[] = esc_html(print_r($post, true));
	// 		$return[] = '<hr>';
	// 		$return[] = esc_html(print_r($meta, true));

	// $parent_term = term_exists( 'fruits', 'product' ); // array is returned if taxonomy is given
	// $parent_term_id = $parent_term['term_id'];         // get numeric term id
	// wp_insert_term(
	//     'Apple',   // the term 
	//     'product', // the taxonomy
	//     array(
	//         'description' => 'A yummy apple.',
	//         'slug'        => 'apple',
	//         'parent'      => $parent_term_id,
	//     )
	// );

	// $return[] = '</pre>';


	return '<textarea style="width: 100%; height: 30em;">' . esc_html(implode("\n", $return)) . '</textarea>';
}


add_shortcode('list-livre', 'list_livre_shortcode');

function list_livre_shortcode($atts, $content, $tag)
{
	$return = [];

	$post_type = 'livre';

	$query = [
		'post_type' => $post_type,
		'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'],
		'posts_per_page' => -1
	];

	$posts = get_posts($query);

	foreach ($posts as $post) {
		$meta = get_post_meta($post->ID, '', true);

		// $return[] = esc_html(print_r($meta, true));

		if ($meta['auteur']) {
			if (strpos($meta['auteur'][0], ':')) {
				$a = unserialize($meta['auteur'][0]);
			} else {
				$a = $meta['auteur'];
			}

			$auteurs = is_array($a) ? implode(',', $a) : $a;
		}

		if ($auteurs) {
			foreach (explode(',', $auteurs) as $id) {
				$from_id = $id;
				$to_id = $post->ID;
				$sql = [];
				$sql[] = "rel_key = 'rel_auteur'";
				$sql[] = "from_type = 'post'";
				$sql[] = "from_status = 'publish'";
				$sql[] = "from_lang = 'fr_FR'";
				$sql[] = "to_type = 'post'";
				$sql[] = "to_status = 'publish'";
				$sql[] = "to_lang = 'fr_FR'";

				$sql[] = "from_name = 'auteur'";
				$sql[] = "from_id = " . $from_id;

				$sql[] = "to_name = '$post_type'";
				$sql[] = "to_id = " . $to_id;

				$return[] = 'INSERT IGNORE INTO wp_p2p_relationships SET ' . implode(", ", $sql) . ';';
			}
		}
	}


	return '<textarea style="width: 100%; height: 30em;">' . esc_html(implode("\n", $return)) . '</textarea>';

	// $parent_term = term_exists( 'fruits', 'product' ); // array is returned if taxonomy is given
	// $parent_term_id = $parent_term['term_id'];         // get numeric term id
	// wp_insert_term(
	//     'Apple',   // the term 
	//     'product', // the taxonomy
	//     array(
	//         'description' => 'A yummy apple.',
	//         'slug'        => 'apple',
	//         'parent'      => $parent_term_id,
	//     )
	// );

}


add_shortcode('list-citation', 'list_citation_shortcode');

function list_citation_shortcode($atts, $content, $tag)
{
	$return = [];

	$post_type = 'citation';

	$query = [
		'post_type' => $post_type,
		'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'],
		'posts_per_page' => -1
	];

	$posts = get_posts($query);

	foreach ($posts as $post) {
		$meta = get_post_meta($post->ID, '', true);

		// $return[] = esc_html(print_r($meta, true));

		if ($meta['auteur_citation']) {
			if (strpos($meta['auteur_citation'][0], ':')) {
				$a = unserialize($meta['auteur_citation'][0]);
			} else {
				$a = $meta['auteur_citation'];
			}

			$auteurs = is_array($a) ? implode(',', $a) : $a;
		}

		if ($auteurs) {
			foreach (explode(',', $auteurs) as $id) {
				$from_id = $id;
				$to_id = $post->ID;
				$sql = [];
				$sql[] = "rel_key = 'rel_auteur'";
				$sql[] = "from_type = 'post'";
				$sql[] = "from_status = 'publish'";
				$sql[] = "from_lang = 'fr_FR'";
				$sql[] = "to_type = 'post'";
				$sql[] = "to_status = 'publish'";
				$sql[] = "to_lang = 'fr_FR'";

				$sql[] = "from_name = 'auteur'";
				$sql[] = "from_id = " . $from_id;

				$sql[] = "to_name = '$post_type'";
				$sql[] = "to_id = " . $to_id;

				$return[] = 'INSERT IGNORE INTO wp_p2p_relationships SET ' . implode(", ", $sql) . ';';
			}
		}
	}


	return '<textarea style="width: 100%; height: 30em;">' . esc_html(implode("\n", $return)) . '</textarea>';

	// $parent_term = term_exists( 'fruits', 'product' ); // array is returned if taxonomy is given
	// $parent_term_id = $parent_term['term_id'];         // get numeric term id
	// wp_insert_term(
	//     'Apple',   // the term 
	//     'product', // the taxonomy
	//     array(
	//         'description' => 'A yummy apple.',
	//         'slug'        => 'apple',
	//         'parent'      => $parent_term_id,
	//     )
	// );

}
