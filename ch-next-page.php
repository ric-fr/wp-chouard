<?php


function ch_next_page_nav()
{
	global $post;

	if (
		$post
		&& $pages = ch_next_page_loop($post->post_type, 0, $post->ID)
	) {
		foreach ($pages as $key => $page) {
			if (
				$page == $post->ID
			) {
				return [
					'prev' => (array_key_exists($key - 1, $pages)) ? $pages[$key - 1] : null,
					'next' => (array_key_exists($key + 1, $pages)) ? $pages[$key + 1] : null,
					// 'pages' => implode(',', $pages), // debug
				];
			}
		}
	}

	return;
}

/**
 * ch_next_page_loop()
 * 
 *  
 * @param string optional $post_type Optional Default: 'page'
 * @param int optional Parent PostID from where
 * @param int optional $stopAt : stop when finds this PostID
 * @param int $i array incremental
 * @param $iStop to stop looping ($i >= $iStop +1)
 * 
 * @return array|null flat ordered array looping over a page hierarchy
 */

function ch_next_page_loop($post_type = 'page', $parent = 0, $stopAt = null, &$i = 0, $iStop = null)
{
	$pages = [];
	$args = [
		'post_type' => $post_type,
		'post_parent' => $parent,
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
	];

	$posts = get_posts($args);

	if ($posts) {
		foreach ($posts as $post) {
			$pages[$i++] = $post->ID;

			if ($iStop && $iStop <= $i)
				return $pages;

			if ($post->ID == $stopAt) {
				$iStop = $i + 1;
			}

			$children = ch_next_page_loop($post_type, $post->ID, $stopAt, $i, $iStop);

			$pages = array_merge(
				$pages,
				$children
			);
		}
	}

	return $pages;
}
