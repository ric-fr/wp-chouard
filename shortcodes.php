<?php

/**
 * [next-page-nav]
 */
// add_shortcode('next-page-nav', 'next_page_nav_shortcode');

// function next_page_nav_shortcode($atts, $content, $tag)
// {
// 	$atts = shortcode_atts([
// 		'post_id' => get_the_ID(),
// 		'post_type' => '',
// 		'class' => 'next-page-nav',
// 		// 'title' => '',
// 		// 'wp_typography' => 1
// 	], $atts, $tag);

// 	$next = null;
// 	$prev = null;

// 	if (
// 		$atts['post_id']
// 		&& $next_id = get_next_page_id($atts['post_id'])
// 	) {
// 		$next = get_post($next_id);
// 		$next_link = get_permalink($next_id);
// 	}

// 	if ($next || $prev) {
// 		$html = '<nav class="' . $atts['class'] . '">';
// 		$html .= '<a href="' . $next_link . '" class="next"';
// 		$html .= ' title="' . esc_attr(wp_strip_all_tags($next->post_title)) . '"';
// 		$html .= '>Suivant</a>';
// 	}
// }

/**
 * [ch-post-tags]
 */

add_shortcode('ch-post-tags', 'ch_post_tags_shortcode');

function ch_post_tags_shortcode($atts, $content, $tag)
{
	$atts = shortcode_atts([
		'post_id' => get_the_ID(),
		'tax' => 'post_tag',
		'class' => 'ch-post-tags',
		'title' => '', // widget title (hidden if no terms found)
		'no_link' => 0, // Set to 1 to disable links to terms
		'title_attr' => 0, // Set to 1 to add title attribute (aka tooltip) on term name (span or a)
		'wp_typography' => 1
	], $atts, $tag);

	if (
		empty($atts['post_id'])
		|| !taxonomy_exists($atts['tax'])
	) {
		return;
	}

	$items = [];

	// if plan_c we point to corresponding plan_p
	if ('plan_c' == $atts['tax']) {
		$terms = get_the_terms($atts['post_id'], $atts['tax']);
		if ($terms) {
			foreach ($terms as $term) {
				$posts = get_posts(
					array(
						'posts_per_page' => -1,
						'post_type' => 'plan_p',
						'tax_query' => array(
							array(
								'taxonomy' => 'plan_c',
								'field' => 'term_id',
								'terms' => $term->term_id,
							)
						)
					)
				);

				foreach ($posts as $post) {
					$e = '';
					$a = [];

					if ($atts['no_link']) {
						$e = 'span';
					} else {
						$e = 'a';
						$a['href'] = get_permalink($post->ID);
					}

					if ($atts['title_attr'])
						$a['title'] = wp_strip_all_tags($post->post_title);

					$item = '<li class="term">';
					$item .= "<$e";

					foreach ($a as $key => $value) {
						$item .= ' ' . $key . '="' . esc_attr($value) . '"';
					}

					$item .= '>' . $post->post_title . "</$e>";

					$item .= '</li>';

					$items[] = $item;
				}
			}
		}
	} else {
		$terms = get_the_terms($atts['post_id'], $atts['tax']);

		if ($terms) {
			foreach ($terms as $term) {
				$e = '';
				$a = [];

				if ($atts['no_link']) {
					$e = 'span';
				} else {
					$e = 'a';
					$a['href'] = get_term_link($term->term_id);
				}

				if ($atts['title_attr'])
					$a['title'] = wp_strip_all_tags($term->name);

				$item = '<li class="term">';
				$item .= "<$e";

				foreach ($a as $key => $value) {
					$item .= ' ' . $key . '="' . esc_attr($value) . '"';
				}

				$item .= '>' . $term->name . "</$e>";

				$item .= '</li>';

				$items[] = $item;

				// $items[] = '<li class="term"><a href="' . get_term_link($term->term_id) . '"'
				// 	. ' title="' . esc_attr($term->name) . '"/>' . $term->name . '</li></a>';
			}
		}
	}

	if ($items) {
		$html = '<aside class="' . $atts['class'] . '">';

		if ($atts['title'] != '') {
			$html .= '<div class="title">' . $atts['title'] . '</div>';
		}

		$html .=  '<ul class="terms">' . implode(' ', $items) . '</ul>';
		$html .=  '</aside>';

		if (
			$atts['wp_typography']
			&& class_exists('WP_Typography')
		) {
			$wp_typo_settings = \WP_Typography::get_user_settings();
			$html = \WP_Typography::process($html, $wp_typo_settings);
		}

		return $html;
	}
}

/**
 * [post-side-nav]
 */

add_shortcode('post-side-nav', 'post_side_nav_shortcode');

function post_side_nav_shortcode($atts, $content, $tag)
{
	$atts = shortcode_atts([
		'id' => null,
		'post_type' => 'any',
		'class' => 'post-side-nav',
		'title' => '',
		'wp_typography' => 1
	], $atts, $tag);

	if ($atts['id']) {
		$id = $atts['id'];
	} else if (is_singular()) {
		$id = get_the_ID();
	}

	if (
		$id &&
		$post = get_post($id)
	) {
		// we continue if attr[post_type] is '' or 'any' or matches current
		if (
			$atts['post_type'] == ''
			|| $atts['post_type'] == 'any'
			|| $atts['post_type'] == $post->post_type
		) {
			// ok, keep going
		} else
			return;

		$html = '<aside class="' . $atts['class'] . '">';

		if ($atts['title'] != '')
			$html .= '<header><span class="title">' . $atts['title'] . '<span></header>';

		$html .= '<nav>' . post_side_nav($id) . '</nav>';

		if (trim($content) != '')
			$html .= '<footer>' . $content . '</footer>';

		$html	.= '</aside>';

		if (
			$atts['wp_typography'] &&
			class_exists('WP_Typography')
		) {
			$wp_typo_settings = \WP_Typography::get_user_settings();
			$html = \WP_Typography::process($html, $wp_typo_settings);
		}

		return $html;
	}

	return;
}




/**
 * Shortcode [post-tree]
 */

add_shortcode('post-tree', 'post_tree_shortcode');

function post_tree_shortcode($atts, $content, $tag)
{
	$atts = shortcode_atts([
		'parent' => 0,
		'post_type' => 'plan_p',
		'limit_depth' => 0, // none
		'description' => 0
	], $atts, $tag);

	$return = [];


	$return[] = post_tree_loop($atts['post_type'], $atts['parent'], $atts);

	return implode("\n", $return);
}

function post_tree_loop($post_type, $parent = 0, $atts = ['limit_depth' => 0], $depth = 0)
{
	$return = [];

	$h = $depth + 3;

	$user = wp_get_current_user();
	$allowed_roles = array('editor', 'administrator', 'author');

	$args = array(
		'post_type' => $post_type,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'post_parent' => $parent
	);

	$posts = query_posts($args);

	if ($posts) {
		$return[] = '<ul class="post-tree">';

		foreach ($posts as $post) {
			$post_link = get_permalink($post->ID);
			$post_edit_link = '/wp-admin/post.php?post=' . $post->ID . '&action=edit';

			$return[] = '<li>';

			if ($h < 7) {
				$return[] = '<h' . $h . ' class="post-title">';
			} else {
				$return[] = '<div class="post-title">';
			}

			$return[] = '<a href="' . $post_link . '">' . $post->post_title . '</a>';

			if (array_intersect($allowed_roles, $user->roles)) {
				$return[] = '<small style="font-size: 60%"><a href="' . $post_edit_link . '">Edit</a></small>';
			}

			if ($h < 7) {
				$return[] = '</h' . $h . '>';
			} else {
				$return[] = '</div>';
			}

			if ($atts['description'] && !empty(trim($post->post_excerpt))) {
				$return[] = '<div class="post-excerpt">' . $post->post_excerpt . '</div>';
			}

			if (
				$post->ID
				&& (
					($atts['limit_depth'] <= 0)
					|| ($atts['limit_depth'] > $depth + 1)
				)
			) {
				$return[] = post_tree_loop($post_type, $post->ID, $atts, $depth + 1);
			}

			$return[] = '</li>';
		}

		$return[] = '</ul>';
	}

	$r = implode("\n", $return);

	if (class_exists('WP_Typography')) {
		$wp_typo_settings = \WP_Typography::get_user_settings();
		$r = \WP_Typography::process($r, $wp_typo_settings);
	}

	return $r;
}


/**
 * Shortcode [term-tree]
 */

add_shortcode('term-tree', 'term_tree_shortcode');

function term_tree_shortcode($atts, $content, $tag)
{
	$atts = shortcode_atts([
		'id' => 0,
		'taxonomy' => 'category',
		'limit_depth' => 0, // none
		'wp_typography' => 0,
		'hide_empty' => 0,
		'description' => 0
	], $atts, $tag);

	$return = [];

	// $return[] = '<style>';
	// $return[] = 'ul.term-tree { list-style-type: none !important; }';
	// $return[] = '</style>';

	$return[] = hierarchical_term_tree($atts['id'], $atts);

	$r = implode("\n", $return);

	if (
		$atts['wp_typography'] &&
		class_exists('WP_Typography')
	) {
		$wp_typo_settings = \WP_Typography::get_user_settings();
		$r = \WP_Typography::process($r, $wp_typo_settings);
	}

	return $r;
}

function hierarchical_term_tree($id = 0, $atts, $depth = 0)
{
	$r = [];

	$h = $depth + 3;

	$children = get_terms([
		'taxonomy' => $atts['taxonomy'],
		'hide_empty' => $atts['hide_empty'],
		'parent' => $id,
	]);

	if ($children) {
		$r[] = '<ul class="term-tree">';

		foreach ($children as $term) {
			$term_link = get_term_link($term->slug, $term->taxonomy);
			$term_edit_link = '/wp-admin/term.php?taxonomy=' . $term->taxonomy . '&tag_ID=' . $term->term_id;

			$r[] = '<li>';

			if ($h < 7) {
				$r[] = '<h' . $h . '>';
			}

			$r[] = '<a href="' . $term_link . '">' . $term->name . ' (' . $term->count . ')</a>';
			$r[] = '<small style="font-size: 60%"><a href="' . $term_edit_link . '">Edit</a></small>';

			if ($h < 7) {
				$r[] = '</h' . $h . '>';
			}

			if (
				$atts['description']
				&& $term->description
			) {
				$r[] = '<div class="description">';
				$r[] = $term->description;
				$r[] = '</div>';
			}

			if (
				$term->term_id 		&&
				(
					($atts['limit_depth'] <= 0)	// unlimited
					|| ($atts['limit_depth'] > $depth + 1) // limit not reached
				)
			) {
				// recursive
				$r[] = hierarchical_term_tree($term->term_id, $atts, $depth + 1);
			}

			$r[] = '</li>';
		}

		$r[] = '</ul>';
	}

	return implode("\n", $r);
}


/**
 * Shortcode [term-tree]
 */

add_shortcode('term-thematique-tree', 'term_thematique_tree_shortcode');

function term_thematique_tree_shortcode($atts, $content, $tag)
{
	$atts = shortcode_atts([
		'id' => 0,
		'taxonomy' => 'thematique',
		'image_size' => 'thumbnail',
		'limit_depth' => 0, // none
		'wp_typography' => 0
	], $atts, $tag);

	$return = [];

	// $return[] = '<style>';
	// $return[] = 'ul.term-tree { list-style-type: none !important; }';
	// $return[] = '</style>';

	$return[] = term_thematique_tree_loop(
		[
			'taxonomy' => $atts['taxonomy'],
			'image_size' => $atts['image_size']
		],
		$atts['id'],
		$atts['limit_depth']
	);

	$html = implode("\n", $return);

	if (
		$atts['wp_typography'] &&
		class_exists('WP_Typography')
	) {
		$wp_typo_settings = \WP_Typography::get_user_settings();
		$html = \WP_Typography::process($html, $wp_typo_settings);
	}

	return $html;
}

function term_thematique_tree_loop($options = [], $id = 0, $limit_depth = 0, $depth = 0)
{
	$taxonomy = $options['taxonomy'] ? $options['taxonomy'] : 'thematique';
	$image_size = $options['image_size'] ? $options['image_size'] : 'thumbnail';

	$r = [];

	$h = $depth + 3;

	$user = wp_get_current_user();
	$allowed_roles = array('editor', 'administrator', 'author');

	$children = get_terms([
		'taxonomy' => $taxonomy,
		'hide_empty' => false,
		'parent' => $id,
	]);

	if ($children) {
		$r[] = '<ul class="term-thematique-tree">';

		foreach ($children as $child) {
			$term_link = get_term_link($child->slug, $child->taxonomy);
			$term_edit_link = '/wp-admin/term.php?taxonomy=' . $child->taxonomy . '&tag_ID=' . $child->term_id;

			$r[] = '<li>';

			if ($h < 7) {
				$r[] = '<h' . $h . ' class="term-title">';
			} else {
				$r[] = '<div class="term-title">';
			}

			$r[] = '<a href="' . $term_link . '">' . $child->name . ' <small class="count">' . $child->count . '</small></a>';

			if (array_intersect($allowed_roles, $user->roles)) {
				$r[] = '<small style="font-size: 60%"><a href="' . $term_edit_link . '">Edit</a></small>';
			}

			if ($h < 7) {
				$r[] = '</h' . $h . '>';
			} else {
				$r[] = '</div>';
			}

			if ($child->description) {
				$r[] = '<div class="term-description">';
				$r[] = $child->description;
				$r[] = '</div>';
			}

			$args = [
				'post_type' => 'livre',
				'posts_per_page' => 50,
				// 'post__not_in' => explode(',', $atts['exclude']),
				'tax_query'      => [
					'relation' => 'AND',
					[
						'taxonomy'  => $taxonomy,
						'field'     => 'slug',
						'terms'     => $child->slug,
						'operator'  => 'IN',
						'include_children' => false
					]
				],
				'suppress_filters'   => false
			];

			if ($child->count > 20) {
				$label = 'Les livres importants';
				$args['tax_query'][] =
					[
						'taxonomy'  => 'importance',
						'field'     => 'slug',
						'terms'     => ['essentiel', 'important'],
						'operator'  => 'IN',
						'include_children' => false
					];
			} else {
				$label = 'Tous les livres';
			}

			if ($thematique_livres = get_posts($args)) {
				$r[] = '<ul class="term-thematique-livres" data-label="' . esc_attr($label) . '">';

				foreach ($thematique_livres as $post) {
					$r[] = '<li>';

					$post_link = get_permalink($post->ID);
					$post_thumb = '';

					if (has_post_thumbnail($post->ID)) {
						$post_thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $image_size);
					}

					$r[] = '<a href="' . $post_link . '"'
						. ' title="' . esc_attr($post->post_title) . '"'
						. '>';

					if ($post_thumb) {
						$r[] = '<img src="' . esc_attr($post_thumb[0]) . '"'
							. ' width="' . $post_thumb[1] . '"'
							. ' height="' . $post_thumb[2] . '"'
							. ' title="' . esc_attr($post->post_title) . '"'
							. ' />';
					} else {
						$r[] = '<div class="title">'
							. esc_html($post->post_title)
							. '</div>';
					}

					$r[] = '</a>';


					$r[] = '</li>';
				}
				$r[] = '</ul>';
			}

			if (
				$child->term_id 		&&
				(
					($limit_depth <= 0)						||
					($limit_depth > $depth + 1)
				)
			) {
				// recursive
				$r[] = term_thematique_tree_loop($options, $child->term_id, $limit_depth, $depth + 1);
			}

			$r[] = '</li>';
		}

		$r[] = '</ul>';
	}

	return implode("\n", $r);
}


// add_shortcode('post-by-term', 'post_by_term_shortcode');

// function post_by_term_shortcode($atts, $content, $tag)
// {
// 	$atts = shortcode_atts([
// 		'slug' => null,
// 		'post_type' => 'post',
// 		'taxonomy' => 'category',
// 		'img' => 1,
// 		'description' => 0, // 0,1
// 		'wp_typography' => 0
// 	], $atts, $tag);
// }


add_shortcode('livres-by-term', 'livres_by_term_shortcode');

$livre_by_term_ids = [];
// atts = attributs
function livres_by_term_shortcode($atts, $content = null, $tag = 'livres_by_term_shortcode')
{
	global $livre_by_term_ids;

	$r = [];

	$atts = shortcode_atts([
		'slug' => '', // taxonomy slug
		'taxonomy' => 'thematique',
		'post_type' => 'livre',
		'limit' => -1,  // -1 = no limit
		'order_by' => 'title',
		'order' => 'ASC',
		'image' => 1,
		'image_size' => 'thumbnail', // thumbnail|medium|large|full
		'description' => 0, // 0,1
		'wp_typography' => 0,
		'importance' => '', // essentiel,important,util,pour-memoire
		'unique' => 0
	], $atts, $tag);

	if (
		$atts['unique']
		// && !array_key_exists($atts['unique'], $livre_by_term_ids)
		&& !is_array($livre_by_term_ids[$atts['unique']])
	)
		$livre_by_term_ids[$atts['unique']] = [];

	$args = [
		'post_type' => $atts['post_type'],
		'posts_per_page' => $atts['limit'],
		'order_by' => $atts['order_by'],
		'order' => $atts['order'],
		'tax_query'      => [
			// 'relation' => 'AND',
		],
		'suppress_filters'   => false
	];

	if ($atts['slug']) {
		$args['tax_query'][] = [
			'taxonomy'  => $atts['taxonomy'],
			'field'     => 'slug',
			'terms'     => explode(',', $atts['slug']),
			'operator'  => 'IN',
			'include_children' => false
		];
	}

	if ($atts['importance']) {
		$args['tax_query'][] = [
			'relation' => 'AND'
		];

		$args['tax_query'][] = [
			'taxonomy'  => 'importance',
			'field'     => 'slug',
			'terms'     => explode(',', $atts['importance']),
			'operator'  => 'IN',
			'include_children' => false
		];
	}

	$posts = get_posts($args);

	if ($posts) {
		$r[] = '<ul class="post-livres">';

		foreach ($posts as $p) {
			$img_src = '';

			$r[] = '<li>';
			if (
				$atts['image']
				&& has_post_thumbnail($p->ID)
				&& $thumb_id = get_post_thumbnail_id($p->ID)
			) {
				$post_thumb = wp_get_attachment_image_src($thumb_id, $atts['image_size']);
			}


			$r[] = '<a href="' . get_permalink($p->ID) . '">';

			if ($post_thumb) {
				$r[] = '<img src="' . esc_attr($post_thumb[0]) . '"'
					. ' width="' . $post_thumb[1] . '"'
					. ' height="' . $post_thumb[2] . '"'
					. ' alt="' . esc_attr($p->post_title) . '"'
					. ' />';
			}
			//			$r[] = '<span class="post-title">' . esc_html($p->post_title) . '</span>'; // ligne pour afficher le titre du livre
			$r[] = '</a>';

			$r[] = '</li>';
		}

		$r[] = '</ul>';
	}

	return implode("\n", $r);
}

/**
 * Calling this shortcode will sync plan_p pages to the plan_c taxonomy
 */

add_shortcode('plan-p-sync', 'plan_p_sync_shortcode');

function plan_p_sync_shortcode($atts)
{
	$r = [];

	// get all plan_p
	$posts = get_posts([
		'post_type' => 'plan_p',
		'posts_per_page' => -1,
		'suppress_filters' => false,
		'post_status' => 'publish'
	]);

	// map array $map_post_term[post_id] = term_id
	$map_post_term = [];

	// display
	$r[] = '<h2>Building map...</h2>';

	// loop on all posts to populate $map_post_term
	foreach ($posts as $post) {
		$terms = wp_get_post_terms($post->ID, 'plan_c');

		if ($n = count($terms) > 1) {
			$r[] = "Warning: post plan_p[$post->ID] $post->post_name has $n matching plan_c. Only plan_c[{$terms[0]->term_id}] will be update."
				. '<br />'
				. '<a href="/wp-admin/post.php?post=' . $post->ID . '&action=edit">Edit ' . $post->post_name . '</a>';
		}

		// if the post has terms plan_c
		if ($terms) {
			// $term_id = $terms[0]->term_id;
			// $r[] = '<pre>' .print_r($terms,true). '</pre>';

			// even if it had many plan_c, we'll update first one only.
			$map_post_term[$post->ID] = $terms[0]->term_id;
		} else {
			$r[] = "Warning: post plan_p[$post->ID] $post->post_name has no matching plan_c"
				. '<br />'
				. '<a href="/wp-admin/post.php?post=' . $post->ID . '&action=edit">Edit ' . $post->post_name . '</a>';
		}
	}

	$r[] = '<h2>Sync plan_c...</h2>';

	// loop on all posts this time to sync matching plan_
	foreach ($posts as $post) {
		if ($map_post_term[$post->ID]) {
			$taxonomy = 'plan_c';
			$term_id = $map_post_term[$post->ID];
			$parent_term_id = ($map_post_term[$post->post_parent]) ?
				$map_post_term[$post->post_parent]
				: 0;

			$term =
				[
					'description' => $post->post_excerpt,
					'parent' => $parent_term_id,
					'slug' => $post->post_name,
					'name' => $post->post_title
				];

			$updated_term_id = wp_update_term(
				$term_id,
				$taxonomy,
				$term
			);

			if ($is_wp_error = is_wp_error($updated_term_id)) {

				$r[] = "Error: plan_p[$post->ID] {$post->post_name}"
					. '<a href="/wp-admin/post.php?post=' . $post->ID . '&action=edit">Edit ' . $post->post_name . '</a>';
				$r[] = "Error: Failed to update $taxonomy[$term_id] parent:{$term['parent']} slug:{$term['slug']}" . print_r($term, true);
				$r[] = "Error: " . print_r($is_wp_error, true);

				// $r[] = $post->post_name . ' - ' . $post->post_excerpt;
				// $r[] = 'post_id:' . $post->ID . ' term_id:' . $map_post_term[$post->ID];
				// $r[] = 'post_parent_id:' . $post->post_parent . ' term_parent_id:' . $map_post_term[$post->post_parent];
			}
		} else {
			// no matching plan_c
			$r[] = "Warning: plan_p[$post->ID] {$post->post_name} does not seem to have a matching plan_c"
				. '<br />'
				. '<a href="/wp-admin/post.php?post=' . $post->ID . '&action=edit">Edit ' . $post->post_name . '</a>';
		}
	}

	$r[] = "<h2>Done.</h2>";

	return '<ul><li>' . implode('</li><li>', $r) . '</li></ul>';
}


add_shortcode('p2p', 'ch_p2p_shortcode');

function ch_p2p_shortcode($atts, $content, $tag)
{
	global $wp_chouard;
	$display_post_ids = [];

	$atts = shortcode_atts([
		'title' => '',

		// Post_IDs Default: curent PostID (self) Ex: 1234 or 1,2,3 or
		//
		'id' => '', // number|n,n|auteur|self|citation|livre multiple comma separated
		'id_magic_via' => '', // rel_auteur|rel_source to replace keywords in id

		'self' => '', // id from where we search threw relations default current postID

		// find relations via p2p_relationships
		'rel' => '', // rel_auteur|rel_livre or other p2p_relationships
		'direction' => 'any', // from_to|to_from|any - p2p_relationships

		// or via taxonomy (rel or tax)
		'tax' => '', // find related via taxonomy
		'tax_operator' => 'AND', // TODO: 'OR' not implementable at this time
		'include_children' => 0, // using tax relation, include children

		'post_type' => 'any', // 'auteur,livre,citation,post,attachement',
		'post_status' => 'publish,inherit',
		'post_format' => '', // video,gallery,audio or -video,-link
		'limit' => 10, // -1 = unlimited
		// attribute max limits the total number to show (ex: multiple auteur could lead to more posts)
		// defaults to 3 * attr[limit]
		'max' => null,
		'orderby' => 'type title',
		'order' => 'ASC',
		'exclude' => 'self,seen',
		'seen' => 'page', // string - seen group name - Set at 0 to disable storing showed post IDs

		'importance' => '',  // filter by importance

		'class' => 'p2p-shortcode',

		'image_size' => 'thumbnail',
		'show_thumb' => 1, // display thumbnail when available. Hide = 0
		// 0 = never show title
		// 1 = display title only when no thumbnail
		// 2 = force display title even when thumbnail exists
		'show_title' => 1,
		'show_excerpt' => 0, // display post excerpt
		'show_content' => 0, // display post full content
		'show_date' => 0, // display post publish date TODO:

		'wp_typography' => 1, // Apply wp_typography filters. 0 = Disabled
	], $atts, $tag);


	$atts['order'] = ('DESC' == strtoupper($atts['order'])) ? 'DESC' : 'ASC';

	// max defaults to 3x atts['limit']
	if ($atts['max'] === null) {
		if ($atts['limit'] == -1) {
			$atts['max'] = -1;
		} else {
			$atts['max'] = $atts['limit'] * 3;
		}
	}

	if ($atts['post_format'] == 'none') {
		$atts['post_format'] = '-video,-aside,-gallery,-link,-image,-quote,-status,-audio,-chat';
	}

	if (is_singular()) {
		// get current post id
		$self_id = ($atts['self'] == '') ? get_the_ID() : $atts['self'];
		$self_post = get_post($self_id);
	}

	// custom post types we magic replace in attr[id]
	$cpts = ['auteur', 'livre', 'citation'];

	if (empty($self_id)) {
		// dirty hack but does the job
		// remove all string from the ids and remplace them by 0
		foreach (array_merge($cpts, ['self']) as $magic) {
			$atts['exclude'] = str_replace($magic, 0, $atts['exclude']);
			$atts['id'] = str_replace($magic, 0, $atts['id']);
		}
	} else {
		$atts['exclude'] = str_replace('self', $self_id, $atts['exclude']);
		$atts['id'] = str_replace('self', $self_id, $atts['id']);

		$attr_id = explode(',', $atts['id']);

		foreach ($cpts as $cpt) {
			// Find current post's 'auteur' or me if I'm an 'auteur'
			// auteur,livre,citation (cpt) linked auteur via 'rel_auteur' the rest via 'rel_source' (p2p).
			if (in_array($cpt, $attr_id)) {
				$get_ids = [];

				if ($self_post->post_type == $cpt) {
					$get_ids[] = $self_id;
				} else {
					if ($atts['id_magic_via'] == '') {
						if (
							$self_post->post_type == 'auteur'
							|| $cpt == 'auteur'
						) {
							$id_magic_via = 'rel_auteur';
						} else {
							$id_magic_via = 'rel_source';
						}
					} else {
						$id_magic_via = $atts['id_magic_via'];
					}

					$get_posts = get_posts([
						'post_type' 				=> $cpt,
						'p2p_rel_key'       => $id_magic_via,	// This is your connection key name. Required.
						'p2p_rel_post_id'   => $self_id,						// The post ID. Inside main loop dont needed.
						'p2p_rel_direction' => 'any',					// The connection direction. 'any' by default. ( 'any' | 'from_to' | 'to_from' )
						'suppress_filters'  => false						// Required
					]);

					if ($get_posts) {
						foreach ($get_posts as $get_post) {
							$get_ids[] = $get_post->ID;
						}

						$atts['id'] = str_replace($cpt, implode(',', $get_ids), $atts['id']);
					} else {
						$atts['id'] = str_replace($cpt, 0, $atts['id']);
					}
				}
			}
		}
	}

	// id defaults to self
	$atts['id'] = (empty($atts['id'])) ? $self_id : $atts['id'];

	if (empty($atts['id'])) {
		// nothing to do
		return;
	}

	if (!empty($atts['rel'])) {
		// nothing to do at this time... we keep going
	} else if (!empty($atts['tax'])) {
		// we need to get the current post ($atts['id']) terms
		// $terms = get_terms([
		// 	'taxonomy' => $atts['tax'],
		// 	'hide_empty' => true,
		// 	'parent' => $id,
		// ]);
		$tax_terms = [];
		foreach (explode(',', $atts['tax']) as $taxonomy) {
			$tax_terms[$taxonomy] = [];

			foreach (get_the_terms($atts['id'], $atts['tax']) as $term) {
				$tax_terms[$taxonomy][] = $term->slug;
			}
		}
	} else {
		// Error: one of rel or tax is required
		return;
	}

	if (
		str_contains($atts['exclude'], 'seen')
		&& !empty($atts['seen'])
	) {
		// create array key for seen group in global $wp_chouard['seen'] if not exists
		if (
			!array_key_exists(
				$atts['seen'], // seen group
				$wp_chouard['seen'] // global
			)
		) {
			$wp_chouard['seen'][$atts['seen']] = [];
		}

		// exclude ids of seen posts 
		$atts['exclude'] = str_replace(
			'seen',
			implode(',', $wp_chouard['seen'][$atts['seen']]),
			$atts['exclude']
		);
	}

	// relation from those ids
	$from_post_ids = explode(',', $atts['id']);

	/**
	 * for each post_id we'll get their p2p relations
	 */
	foreach ($from_post_ids as $post_id) {
		$args = [
			'post__not_in' 			=> explode(',', $atts['exclude']),
			'posts_per_page' 		=> $atts['limit'],
			'orderby' 					=> $atts['orderby'],
			'order' 						=> $atts['order'],
			'limit' 						=> $atts['limit'],
			'post_type' 				=> $atts['post_type'],
			'suppress_filters'  => false,		// Required for p2p_relationships
			'tax_query' 				=> []
		];

		if (!empty($atts['rel'])) {
			// relations based on p2p_relationships

			$args['p2p_rel_post_id'] = $post_id;	// The post ID. Inside main loop dont needed.
			$args['p2p_rel_key'] = $atts['rel']; 	// This is your connection key name. Required.
			$args['p2p_rel_direction'] = $atts['direction'];	// The connection direction. 'any' by default. ( 'any' | 'from_to' | 'to_from' )

		} else if (!empty($atts['tax'])) {
			// relations based on taxonomies
			if (count($tax_terms) > 1) {
				// should be set only if we have more than one tax_queries
				// and sadly we would want OR ... but then importance is OR too :'(
				$args['tax_query']['relation'] = 'OR';
			}

			foreach ($tax_terms as $taxonomy => $terms) {
				$args['tax_query'][] = [
					'taxonomy' => $taxonomy,
					'field' => 'slug',
					'terms' => $terms,
					'include_children' => $atts['include_children'],
					// 'operator' => 'IN' // this is the default
				];
			}
		} else {
			return 'Error: one of attribute rel or tax is required';
		}

		if (!empty($atts['post_status'])) {
			$args['post_status'] = explode(',', $atts['post_status']);
		}

		/**
		 * only those formats
		 * post_format=video,quote
		 * 
		 * exclude those formats
		 * post_format=-video,-audio
		 */

		$formats = [];
		$exclude_formats = [];

		// split include/exclude formats
		if (
			!empty($atts['post_format'])
			&& $post_formats = explode(',', $atts['post_format'])
		) {
			foreach ($post_formats as $format) {
				// starts with '-' = exclude
				if (substr($format, 0, 1) == '-') {
					$exclude_formats[] = 'post-format-' . substr($format, 1);
				} else {
					$formats[] = 'post-format-' . $format;
				}
			}
		}

		// add relevant term query for post_format
		if (!empty($formats)) {
			$args['tax_query']['relation'] = 'AND';

			$args['tax_query'][] = [
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => $formats,
				// 'terms' => 'post-format-' . $atts['post_format'],
				// 'operator' => 'IN' // this is the default
			];
		}

		if (!empty($exclude_formats)) {
			$args['tax_query']['relation'] = 'AND';

			$args['tax_query'][] = [
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => $exclude_formats,
				'operator' => 'NOT IN'
			];
		}

		if ($atts['exclude']) {
			$args['post__not_in'] = explode(',', $atts['exclude']);
		}

		if (!empty($atts['importance'])) {
			$args['tax_query']['relation'] = 'AND';

			$args['tax_query'][] = [
				[
					'taxonomy'  => 'importance',
					'field'     => 'slug',
					'terms'     => explode(',', $atts['importance']),
					'include_children' => false
					// 'operator'  => 'IN',
				],
			];
		}

		$posts = get_posts($args);

		foreach ($posts as $post) {
			$display_post_ids[] = $post->ID;
		}
	}

	// ‘publish‘ – a published post or page.
	// ‘pending‘ – post is pending review.
	// ‘draft‘ – a post in draft status.
	// ‘auto-draft‘ – a newly created post, with no content.
	// ‘future‘ – a post to publish in the future.
	// ‘private‘ – not visible to users who are not logged in.
	// ‘inherit‘

	$options = [
		'post__in' => implode(',', $display_post_ids),
		'post_type' => $atts['post_type'], //'any',
		'post_status' => 'publish,inherit',
		'class' => $atts['class'],
		'image_size' => $atts['image_size'],
		'orderby' => $atts['orderby'],
		'order' => $atts['order'],
		'title' => $atts['title'],
		'posts_per_page' => $atts['max'],
		'seen' => $atts['seen'],
		'show_thumb'		=> $atts['show_thumb'],
		'show_title'		=> $atts['show_title'],
		'show_excerpt'  => $atts['show_excerpt'],
		'show_content'  => $atts['show_content'],
		'show_date'			=> $atts['show_date'],
		'wp_typography' => $atts['wp_typography'],
	];

	return ch_posts_shortcode($options, $content, $tag);
}


add_shortcode('ch-posts', 'ch_posts_shortcode');

function ch_posts_shortcode($atts, $content = null, $tag = null)
{
	global $wp_chouard;

	$return = [];

	$atts = shortcode_atts([
		'title' 				=> '',
		'post_type' 		=> get_post_types(), // any 'auteur,livre,citation,post,attachment',
		'post_status' 	=> 'publish,inherit',
		'post__in'    	=> '',
		'posts_per_page' => 10,
		'orderby' 			=> 'date',
		'order' 				=> 'ASC',
		'class' 				=> 'ch-posts',
		'image_size' 		=> 'thumbnail',
		'seen' 					=> 'page',

		'show_thumb'		=> 1, // display thumbnail when available. Hide = 0
		// 0 = never show title
		// 1 = display title only when no thumbnail
		// 2 = force display title even when thumbnail exists
		'show_title'		=> 1,
		'show_excerpt'  => 0, // display post excerpt
		'show_content'  => 0, // display post full content
		'show_date'			=> 0, // display post publish date TODO:

		'wp_typography' => 1, // Apply wp_typography filters. Set to 0 to disable.
	], $atts, $tag);

	$args = [
		'posts_per_page' => $atts['posts_per_page'],
		'suppress_filters'   => false
	];

	if (count(explode(',', $atts['post__in']))) {
		$args['post__in'] = explode(',', $atts['post__in']);
	} else {
		return;
	}

	if (!empty($atts['post_type']))
		$args['post_type'] = explode(',', $atts['post_type']);

	if (!empty($atts['post_status']))
		$args['post_status'] = explode(',', $atts['post_status']);

	if (!empty($atts['orderby']))
		$args['orderby'] = $atts['orderby'];

	if (!empty($atts['order']))
		$args['order'] = $atts['order'];


	// create empty seen array if needed
	if (
		!empty($atts['seen'])
		&& !array_key_exists($atts['seen'], $wp_chouard['seen'])
	) {
		$wp_chouard['seen'][$atts['seen']] = [];
	}

	$posts = get_posts($args);

	if ($posts) {
		// get all available post types
		$post_types = get_post_types([
			// 'name' => $post->post_type
		], 'objects');

		$items = [];

		foreach ($posts as $post) {
			$post_class = [];
			// $post_tax = [];

			$has_image = false;
			$thumb_src = '';
			$post_img = ''; // html <img />

			$post_link_attributes = [];

			/**
			 * link pdf directly to file not WordPress permalink
			 */
			if ($post->post_mime_type == 'application/pdf') {
				$post_link = wp_get_attachment_url($post->ID);
				$post_link_attributes['target'] = '_blank';
			} else {
				$post_link = get_permalink($post->ID);
			}

			$post_format = get_post_format($post);

			// if (has_post_format('video', $post->ID)) {
			// 	// Do something
			// }

			// get available taxonomies for this post type
			$taxonomies = get_object_taxonomies($post->post_type);

			foreach ($taxonomies as $taxonomie) {
				$post_taxonomies[$taxonomie] = wp_get_post_terms($post->ID, $taxonomie);
			}

			// // get the post format (video,aside,etc...)
			// $post_tax['post_format'] = wp_get_post_terms($post->ID, 'post_format');

			$post_class[] = 'item';
			$post_class[] = 'post-type-' . $post->post_type;

			// taxonomies we want a class added for
			foreach (['importance'] as $tax) {
				if ($post_taxonomies[$tax]) {
					foreach ($post_taxonomies[$tax] as $term) {
						$post_class[] = $tax . '-' . $term->slug;
					}
				}
			}

			$post_data = [
				'id' => $post->ID,
				'type' => $post->post_type,
				'title' => $post->post_title,
				'date' => date_i18n('l j F Y \à G\h:m', strtotime($post->post_date)),
				// 'date' => date(get_option('date_format'), strtotime($post->post_date)),
				// 'date-utc' => date('c', strtotime($post->post_date_utc)),
				'format' => $post_format,
			];

			switch ($post_format) {
				case 'video':
					$post_data['label'] = 'Vidéo';
					break;
				case 'quote':
					$post_data['label'] = 'Citation';
					break;
				default:
					switch ($post->post_type) {
						case 'attachment':
							if ($post->post_mime_type == 'application/pdf') {
								$post_data['label'] = 'Pdf';
							} else if (str_starts_with($post->post_mime_type, 'image')) {
								$post_data['label'] = 'Image';
							} else if (str_starts_with($post->post_mime_type, 'application')) {
								$post_data['label'] = 'Doc';
							} else {
								$post_data['label'] = 'Média';
							}
							break;
						default:
							$post_data['label'] = $post_types[$post->post_type]->labels->singular_name;
					}
			}

			if ($post_taxonomies['importance']) {
				$importance_names = [];

				foreach ($post_taxonomies['importance'] as $term) {
					$importance_names[] = $term->name;
				}
				$post_data['importance'] = implode(' ', $importance_names);
			}

			if ($atts['show_thumb']) {

				$thumb_id = ($post->post_type == 'attachment') ?
					$post->ID
					: get_post_thumbnail_id($post->ID);

				if (
					$thumb_id
					&& $thumb = wp_get_attachment_image_src($thumb_id, $atts['image_size'])
				) {
					$has_image = true;
					$thumb_src = $thumb[0];
					$post_img = '<img src="' . esc_attr($thumb_src) . '"'
						. ' width="' . $thumb[1] . '"'
						. ' height="' . $thumb[2] . '"'
						. ' alt="' . esc_attr($post->post_title) . '"'
						. ' />';
				}
			}

			if ($has_image) {
				$post_class[] = 'has-image';
			}

			// show_title = 2 => force
			// = 1 => show if no thumb
			// = 0 => hide
			if (
				$atts['show_title'] > 1
				|| ($atts['show_title']
					&& !$has_image)
			) {
				// if we have a thumbnail, we hide title
				$show_title = true;
			} else {
				$show_title = false;
			}

			$post_link_attributes['title'] = $post_data['label'] . ' : ' . $post->post_title;

			$r = '<a href="' . $post_link . '"';
			foreach ($post_link_attributes as $key => $value) {
				$r	.= ' ' . $key . '="' . esc_attr($value) . '"';
			}
			$r	.= '>';

			if (
				$has_image
				&& !empty($post_img)
			) {
				$r .= '<div class="item-thumb">' . $post_img . '</div>';
			}

			if ($show_title) {
				$r	.= '<div class="item-title">'
					. esc_html($post->post_title)
					. '</div>';
			}

			// get_the_content( string $more_link_text = null, bool $strip_teaser = false, WP_Post|object|int $post = null )

			if ($atts['show_content']) {
				// $more_link_text = '+';
				// $strip_teaser = false;
				// $content = get_the_content($more_link_text, $strip_teaser, $post);

				$content = 	apply_filters('the_content', $post->post_content);
				$r	.= '<div class="item-content">'
					. $content
					. '</div>';
			} else if ($atts['show_excerpt']) {
				$excerpt = trim(get_the_excerpt($post));

				if ($excerpt == '') {
					// try to build excerpt from post content?
				}

				$r	.= '<div class="item-excerpt">'
					. $excerpt
					. '</div>';
			}

			$r	.= '</a>';


			$item = '<li class="' . implode(' ', $post_class) . '"';

			foreach ($post_data as $key => $data) {
				if ($data != '') {
					$item .= ' data-' . $key . '="' . esc_attr($data) . '"';
				}
			}

			$item .= '>'
				. $r
				. '</li>';

			$items[] = $item;

			// store post_id to not be displayed twice on same page or seen group (default 'page')
			// set to seen=0 to disable
			if (!empty($atts['seen'])) {
				$wp_chouard['seen'][$atts['seen']][] = $post->ID;
			}
		}

		$return[] = '<aside class="' . esc_attr($atts['class']) . '">';

		if (!empty($atts['title'])) {
			$return[] = '<header>';
			$return[] = '<span class="title">' . $atts['title'] . '<span>';
			$return[] = '</header>';
		}

		$return[] = '<ul class="items">';
		$return[] = implode("\n", $items);
		$return[] = '</ul>';

		if (trim($content) != '') {
			$return[] = '<footer>';
			$return[] = $content;
			$return[] = '</footer>';
		}

		$return[] = '</aside>';

		$html = implode("\n", $return);

		/**
		 * WP_Typography plugin activated, we filter.
		 * controlled by attribute WP_Typography = 1|0 Default: 1
		 */
		if (
			$atts['wp_typography']
			&& class_exists('WP_Typography')
		) {
			$wp_typo_settings = \WP_Typography::get_user_settings();
			$html = \WP_Typography::process($html, $wp_typo_settings);
		}

		return $html;
	}
}
