<?php

// function audio_only( $query ) {
// 	if ( $query->is_home() && $query->is_main_query() ) {
// 			$query->set( 'post_format', 'post-format-audio' );
// 	}
// }
// add_action( 'pre_get_posts', 'audio_only' );

// get_first_embed_media(1234, '/video|youtube|vimeo/')
function get_first_embed_media($post_id, $regex_filter = null)
{
	$post = get_post($post_id);
	$content = do_shortcode(apply_filters('the_content', $post->post_content));
	$embeds = get_media_embedded_in_content($content);

	if (!empty($embeds)) {
		// Loop
		foreach ($embeds as $embed) {
			if (
				$regex_filter == null 						||
				preg_match($regex_filter, $embed)
			) {
				return $embed;
			}
		}
	}

	// No matching embedded found
	return false;
}

if (!function_exists('str_starts_with')) {
	/**
	 * Php 8 polyfill
	 * @param string $string
	 * @param string $query
	 */

	function str_starts_with($string, $query)
	{
		return substr($string, 0, strlen($query)) === $query;
	}
}


// function get_next_page_id($post_id, $next = 1)
// {
// 	global $post, $wpdb;
// 	if ($post_id === null)
// 		$p = $post;
// 	else
// 		$p = get_post($post_id);

// 	$next_id = $wpdb->get_var(
// 		$wpdb->prepare("SELECT ID next_id
// 		FROM {$wpdb->posts}
// 		WHERE post_status = 'publish'
// 			AND post_type = %s
// 			AND post_parent = %d
// 			AND menu_order > %d
// 			ORDER BY menu_order ASC, ID ASC
// 			LIMIT 1
// 			", $post->post_type, $p->post_parent, $p->menu_order)
// 	);

// 	if(!$next_id) {
		
// 	}

// 	return ($res_id) ? $res_id : null;
// }
