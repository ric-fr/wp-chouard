<?php

/**
 * used for plan-p-breadcrumb
 */

function post_side_nav($id = null)
{
	$return = [];

	if ($id == null && is_singular()) {
		$id = get_the_ID();
	}

	if ($id) {
		$post = get_post($id);
	}

	if (!$post) {
		return;
	}

	$return[] = '<ul>';

	if (
		$post->post_parent &&
		$post_parent = get_post($post->post_parent)
	) {
		$return[] = '<li class="parent">';
		$return[] = '<a href="' . get_permalink($post_parent->ID) . '"'
			. '>' . $post_parent->post_title . '</a>';
		$return[] = '</li>';
	}

	$post_siblings = get_posts([
		'post_type' => $post->post_type,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'post_parent' => $post->post_parent,
		'suppress_filters'   => false
	]);

	if ($post_siblings) {
		foreach ($post_siblings as $post_sibling) {
			$return[] = '<li class="' . (($post_sibling->ID == $post->ID) ? 'current' : '') . '">';
			$return[] = '<a href="' . get_permalink($post_sibling->ID) . '">' . $post_sibling->post_title . '</a>';

			if ($post_sibling->ID == $post->ID) {
				// this is current page
				// we fetch children

				$post_children = get_posts([
					'post_type' => 'plan_p',
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'posts_per_page' => -1,
					'post_parent' => $post_sibling->ID,
					'suppress_filters'   => false
				]);

				if ($post_children) {
					$return[] = '<ul>';

					foreach ($post_children as $post_child) {
						$return[] = '<li>'
							. '<a href="' . get_permalink($post_child->ID) . '">' . $post_child->post_title . '</a>'
							. '</li>';
					}

					$return[] = '</ul>';
				}
			}

			$return[] = '</li>';
		}
	}

	$return[] = '</ul>';

	return implode("\n", $return);
}
