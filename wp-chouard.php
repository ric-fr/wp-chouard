<?php

/*
Plugin Name: WP Chouard
Plugin URI: https://www.chouard.org/
Description: WP Chouard toolbox
Version: 0.0.1
Author: RIC France
Author URI: https://www.choaurd.org/
License: GPLv2 or later
Text Domain: wp-chouard
*/

/************
 * v0.0.1 : init
 ************/

// Disable direct access
defined('ABSPATH') or die('No script kiddies please!');

// global
$wp_chouard = [];
// $wp_chouard['seen']['page'] seen post IDs on page group (see p2p_shortcode)
$wp_chouard['seen'] = [];

include_once('theia-sticky-sidebar.php');
include_once('divi-custom.php');
include_once('utils.php');
include_once('ch-next-page.php');
include_once('post-side-nav.php');
include_once('shortcodes.php');
include_once('filters.php');
include_once('obsolete.php');

// register js and style on initialization
add_action('init', 'register_wp_chouard');
function register_wp_chouard()
{
	wp_register_script('wp-chouard-script', plugins_url('/script.js', __FILE__), ['jquery'], file_timestamp('/script.js'));
	wp_register_style('wp-chouard-style', plugins_url('/style.css', __FILE__), false, file_timestamp('/style.css'), 'all');
}

// use the registered js and style above
add_action('wp_enqueue_scripts', 'enqueue_wp_chouard');
function enqueue_wp_chouard()
{
	wp_enqueue_script('wp-chouard-script');
	wp_enqueue_style('wp-chouard-style');
}

//if (taxonomy_exists('question_category'))

/**
 * Active les Categories et les Tags natifs de WordPress sur des 'custom post type'
 */
// add_action('init', function () {
// 	register_taxonomy_for_object_type('category', 'custom_post_type');
// 	register_taxonomy_for_object_type('post_tag', 'custom_post_type');
// }, 10, 99);


function post_or_category_descendant_of($category_id)
{
	if (is_category() && (is_category($category_id) || cat_is_ancestor_of($category_id, get_queried_object()))) {
		return true;
	} else if (is_singular()) {

		$post_id = get_queried_object()->id;
		$post_categories = get_the_category($post_id);

		$post_cat_ids = [];

		foreach ($post_categories as $pc) {
			$post_cat_ids[] = $pc->cat_ID;
		}

		$args = array('child_of' => $category_id, 'fields' => 'ids');
		$target_cat_ids = get_categories($args);
		$target_cat_ids[] = $category_id;

		if (array_intersect($post_cat_ids, $target_cat_ids)) {
			return true;
		} else {
			return false;
		}
	}

	// else
	return false;
}

function file_timestamp($file, $fallback = 'time()')
{
	if ('time()' === $fallback) {
		$fallback = time();
	}

	// $file_path = plugins_url($file, __FILE__);
	$file_path = __DIR__ . $file;

	return (file_exists($file_path)) ? filemtime($file_path) : $fallback;
}
