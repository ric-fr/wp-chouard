<?php

// 	'terms' => array( 
//                 'post-format-aside',
//                 'post-format-audio',
//                 'post-format-chat',
//                 'post-format-gallery',
//                 'post-format-image',
//                 'post-format-link',
//                 'post-format-quote',
//                 'post-format-status',
//                 'post-format-video'
//             ),

// return wp_link_pages([
// 	'echo' => 0,
// 	'child_of' => $post->post_parent,
// 	'post_type' => 'plan_p'
// ]);

// hack vscode code check
class WP_Typography
{
	static function get_user_settings()
	{
	}
	static function process($a, $b)
	{
		return '';
	}
}


// use page slug to call dflip book in same slug dflip_category
add_shortcode('dflip_book_slug', 'dflip_book_slug_shortcode');
function dflip_book_slug_shortcode($atts = array(), $content = '', $tag)
{
	global $post;

	// default parameters (but filtered, only declared ones are passed)
	// $atts = shortcode_atts(array(
	//   'books' => $post->post_name
	// ), $atts, $tag);

	$atts = is_array($atts) ? $atts : array();

	if (array_key_exists('books', $atts)) {
		$atts['books'] = str_replace('%slug%', $post->post_name, $atts['books']);
	} else {
		// par défaut on passe le slug
		$atts['books'] = $post->post_name;
	}

	$atts_str = array();
	foreach ($atts as $key => $value) {
		$atts_str[] = $key . '="' . str_replace('"', '\"', $value) . '"';
	}

	$shortcode = '[dflip ' . implode(' ', $atts_str) . ']' . $content . '[/dflip]';
	return do_shortcode($shortcode);
}

// add_filter('shortcode_atts_dflip', 'chouard_filter_dflip_shortcode', 10, 4);
// function chouard_filter_dflip_shortcode($out, $pairs, $atts, $shortcode)
// {

//   return $out;
// };



function hierarchical_term_tree_trash($category = 0)
{
	$r = [];

	$next = get_terms([
		'taxonomy' => 'category',
		'hide_empty' => false,
		'parent' => $category,
	]);

	if ($next) {
		$r[] = '<ul>';

		foreach ($next as $cat) {
			$term_link = get_term_link($cat->slug, $cat->taxonomy);
			$term_edit_link = '/wp-admin/term.php?taxonomy=' . $cat->taxonomy . '&tag_ID=' . $cat->term_id;

			$r[] = '<li>';
			$r[] = '<a href="' . $term_link . '">' . esc_html($cat->name) . ' (' . $cat->count . ')</a>';
			$r[] = '<small style="font-size: 60%"><a href="' . $term_edit_link . '">Edit</a></small>';

			if ($cat->term_id) {
				$r[] = hierarchical_term_tree($cat->term_id, []);
			}

			$r[] = '</li>';
		}

		$r[] = '</ul>';
	}

	return implode("\n", $r);
}



$post_types = get_post_types([
	// 'name' => $post->post_type
], 'objects');

// foreach ( $post_types  as $post_type ) {
// 	echo '<p>Custom Post Type name: ' . $post_type->name . "<br />\n";
// 	echo 'Single name: ' . $post_type->labels->singular_name . "<br />\n";
// 	echo 'Menu icon URL: ' . $post_type->menu_icon . "</p>\n";;
// }


/**
 * Shortcode [list-cat]
 */


add_shortcode('list-cat', 'list_cat_shortcode');

function list_cat_shortcode($atts, $content, $tag)
{
	$atts = shortcode_atts(array(
		'id' => null
	), $atts, $tag);

	$return = [];


	$root_categories = get_terms([
		'taxonomy' => 'category',
		'hide_empty' => false,
		'parent' => 0 // or 
		//'child_of' => 17 // to target not only direct children
	]);

	foreach ($root_categories as $root_category) {
		$children = [];

		$return[] = '<h2>' . $root_category->name . '</h2>';
		$children[] = $root_category->term_id;

		$categories = get_terms([
			'taxonomy' => 'category',
			'hide_empty' => false,
			'child_of' => $root_category->term_id
		]);
		foreach ($categories as $category) {
			$children[] = $category->term_id;
		}
		$return[] = '<p>' . implode(', ', $children) . '</p>';
	}

	$return[] = hierarchical_term_tree(0, $atts);

	$r = implode("\n", $return);

	// if (class_exists('WP_Typography')) {
	// 	$wp_typo_settings = \WP_Typography::get_user_settings();
	// 	$r = \WP_Typography::process($r, $wp_typo_settings);
	// }

	return $r;
}
