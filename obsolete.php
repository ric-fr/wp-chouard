<?php


/**
 * Shortcode [auteur-livre]
 */

add_shortcode('auteur-livre', 'rel_auteur_shortcode');

/**
 * @param id id post_id
 * @param post_type default: any exemple: post,attachement
 * @param limit default: -1 (unlimited)
 * @param orderby default: type title
 * @param order default: ASC
 * 
 * @obsolete
 */

function rel_auteur_shortcode($atts, $content, $tag)
{
	$return = [];

	$atts = shortcode_atts([
		'id' => '', // post_id
		'post_type' => 'any', // 'auteur,livre,citation,post,attachement',
		'limit' => -1, // none
		'orderby' => 'type title',
		'order' => 'ASC',
		'exclude' => 'self'
	], $atts, $tag);

	if (!empty($atts['id'])) {
		$id = $atts['id'];
	} else if (is_singular()) {
		$id = get_the_ID();
	}

	if (!$id)
		return;

	if (
		$atts['exclude']
		&& is_singular()
	) {
		$atts['exclude'] = str_replace('self', get_the_ID(), $atts['exclude']);
	}

	$posts = get_posts([
		'post__not_in' => explode(',', $atts['exclude']),
		'posts_per_page' => $atts['limit'],
		'orderby' => $atts['orderby'],
		'order' => $atts['order'],
		'post_type' => explode(',', $atts['post_type']),
		'p2p_rel_key'        => 'rel_auteur',	// This is your connection key name. Required.
		'p2p_rel_post_id'    => $id,						// The post ID. Inside main loop dont needed.
		'p2p_rel_direction'  => 'any',					// The connection direction. 'any' by default. ( 'any' | 'from_to' | 'to_from' )
		'suppress_filters'   => false						// Required
	]);

	$post_types = get_post_types([
		// 'name' => $post->post_type
	], 'objects');

	if ($posts) {
		foreach ($posts as $post) {
			$post_link = get_permalink($post->ID);
			$post_thumb = '';

			// $return[] = '<pre>' . print_r($post_types, true) . '</pre>';

			if (has_post_thumbnail($post->ID)) {
				$post_thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
			}

			$r = '<a href="' . $post_link . '"'
				. ' title="' . esc_html($post_types[$post->post_type]->labels->singular_name) . ' : ' . esc_attr($post->post_title) . '"'
				. '>';

			if ($post_thumb) {
				$r .= '<img src="' . esc_attr($post_thumb[0]) . '"'
					. ' width="' . $post_thumb[1] . '"'
					. ' height="' . $post_thumb[2] . '"'
					. ' />';
			} else {
				$r	.= '<div class="title">'
					. esc_html($post->post_title)
					. '</div>';
			}

			$return[] = '<li class="post-type-' . $post->post_type . '">' . $r . '</li>';
		}
	}

	$r = '<ul class="auteur-livre">' . implode("\n", $return) . '</ul>';

	if (class_exists('WP_Typography')) {
		$wp_typo_settings = \WP_Typography::get_user_settings();
		$r = \WP_Typography::process($r, $wp_typo_settings);
	}

	return $r;
}



add_shortcode('livre-meme-auteur', 'livre_meme_auteur_shortcode');

function livre_meme_auteur_shortcode($atts, $content, $tag)
{
	$return = [];

	$atts = shortcode_atts([
		'id' => '', // post_id
		'post_type' => 'livre', // 'auteur,livre,citation,post,attachement',
		'limit' => -1, // none
		'orderby' => 'type,title',
		'order' => 'ASC'
	], $atts, $tag);

	if (!empty($atts['id'])) {
		$id = $atts['id'];
	} else if (is_singular()) {
		$id = get_the_ID();
	}

	if (!$id)
		return;

	$auteurs = get_posts([
		'post_type' => 'auteur',
		'p2p_rel_key'        => 'rel_auteur',	// This is your connection key name. Required.
		'p2p_rel_post_id'    => $id,						// The post ID. Inside main loop dont needed.
		'p2p_rel_direction'  => 'any',					// The connection direction. 'any' by default. ( 'any' | 'from_to' | 'to_from' )
		'suppress_filters'   => false						// Required
	]);

	if (!$auteurs)
		return;

	foreach ($auteurs as $auteur) {
		$atts2 = [
			'id' => $auteur->ID, // post_id
			'post_type' => $atts['post_type'], // 'auteur,livre,citation,post,attachement',
			'limit' => $atts['limit'],
			'orderby' => $atts['orderby'],
			'order' => $atts['order']
		];

		$return[] = rel_auteur_shortcode($atts2, $content, 'auteur-livre');
	}



	return join("\n", $return);
}







/**
 * Shortcode [tax-related]
 * @param taxonomy default: plan_c
 * 
 */

add_shortcode('tax-related', 'tax_related_shortcode');

// store yet displayed ids to avoid duplicates
$tax_related_seen = [];

function tax_related_shortcode($atts, $content, $tag)
{
	global $tax_related_seen;

	$return = [];

	$atts = shortcode_atts([
		'id' => null,
		'post_type' => 'post,livre,auteur,attachment',
		'taxonomy' => 'plan_c',
		'importance' => '',
		'post_format' => '',
		'post_status' => '',
		'include_children' => '0',
		'class' => 'tax-related',
		'limit' => 10,
		'orderby' => '',
		'order' => '',
		'exclude' => 'self,seen',
		'image_size' => 'medium'
	], $atts, $tag);

	if ($atts['id']) {
		$id = $atts['id'];
	} else if (is_singular()) {
		// get current post id
		$id = get_the_ID();
	}

	if (
		$id &&
		// get term related to curent post
		$taxonomies = get_the_terms($id, $atts['taxonomy'])
	) {
		$taxonomie_slugs = [];

		foreach ($taxonomies as $taxonomie) {
			$taxonomie_slugs[] = $taxonomie->slug;
		}

		// le post courant n'a pas de lien via plan_c
		if (!$taxonomie_slugs)
			return;

		$atts['exclude'] = str_replace('self', $id, $atts['exclude']);

		$atts['exclude'] = trim(
			str_replace(
				'seen',
				implode(',', $tax_related_seen),
				$atts['exclude']
			),
			','
		);

		$args = [
			'post_type' => explode(',', $atts['post_type']),
			'posts_per_page' => $atts['limit'],
			'post__not_in' => explode(',', $atts['exclude']),
			'tax_query'      => [
				'relation' => 'AND',
			],
			'suppress_filters'   => false
		];

		$args['tax_query'][] = [
			[
				'taxonomy'  => 'plan_c',
				'field'     => 'slug',
				'terms'     => $taxonomie_slugs,
				'operator'  => 'IN',
				'include_children' => $atts['include_children'] ? true : false
			],
		];

		if ($atts['importance']) {
			$args['tax_query'][] = [
				[
					'taxonomy'  => 'importance',
					'field'     => 'slug',
					'terms'     => explode(',', $atts['importance']),
					'operator'  => 'IN',
					'include_children' => false
				],
			];
		}

		if ($atts['orderby']) {
			$args['orderby'] = $atts['orderby'];
		}

		if ($atts['order']) {
			$args['order'] = $atts['order'];
		}

		if ($atts['post_type'] == 'video') {
			$args['post_type'] = 'post';
			// $args['tax_query']['relation'] = 'AND';
			$args['tax_query'][] = [
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => 'post-format-video'
			];
		}

		if ( // attachement default post_status=inherit
			$atts['post_type'] == 'attachment'
			&& !$atts['post_status']
		) {
			$atts['post_status'] = 'inherit';
		}

		if ($atts['post_status']) {
			$args['post_status'] = $atts['post_status'];
		}

		$posts = get_posts($args);

		if ($posts) {
			$return[] = '<ul class="' . esc_attr($atts['class']) . '">';

			foreach ($posts as $post) {
				// store post_id to not be displayed twice on same page
				$tax_related_seen[] = $post->ID;

				$post_link = get_permalink($post->ID);
				$post_thumb = '';

				if (has_post_thumbnail($post->ID)) {
					$post_thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $atts['image_size']);
				}

				$r = '<a href="' . $post_link . '"'
					. ' title="' . esc_attr($post->post_title) . '"'
					. '>';

				if ($post_thumb) {
					$r .= '<img src="' . esc_attr($post_thumb[0]) . '"'
						. ' width="' . $post_thumb[1] . '"'
						. ' height="' . $post_thumb[2] . '"'
						. ' title="' . esc_attr($post->post_title) . '"'
						. ' />';
				} else if (
					$post->post_type = 'attachment'
					&& $attachment_thumb = wp_get_attachment_thumb_url($post->ID)
				) {
					$r .= '<img src="' . $attachment_thumb . '"'
						. ' title="' . esc_attr($post->post_title) . '"'
						. '/>';
				} else {
					$r	.= '<div class="title">'
						. esc_html($post->post_title)
						. '</div>';
				}

				$r	.= '</a>';

				$return[] = '<li class="post-type-' . $post->post_type . '">' . $r . '</li>';
			}

			$return[] = '</ul>';
		}
	}

	return implode("\n", $return);
}
