# Chouard.org Plan C

## !!! Work in progress !!!

In active development...

---

# Shortcode `[ch-post-tags]`

Liste les étiquettes (ou autre termes/catégories) d'un post

**Remarque** : avec la taxonomie `plan_c` le lien se fera vers les pages `plan_p` correspondantes (Parties Plan).

## Usage

```php

// Uniquement le nom des étiquettes, pas de lien <a>
[ch-post-tags title="Importance" tax="importance" no_link=1]

// les liens fournis seront vers les Pages du Plan et non vers les plan_c
[ch-post-tags title="Parties du plan" tax="plan_c" class="post-tags-plan"]

// lister les tags en ajoutant l'attribut title (aka tooltip)
[ch-post-tags title_attr=1]

```

## Attributes

### `tax`

Get the terms for `tax` taxonomy.

Default: 'post_tag' (Étiquettes)

```php

[ch-post-tags title="Tags" title_attr=1]

[ch-post-tags tax="sujet" title="Sujets"]

[ch-post-tags tax="category" title="Catégories"]

[ch-post-tags tax="thematique" title="Thématiques"]

[ch-post-tags tax="plan_c" title="Parties du Plan"]

[ch-post-tags tax="importance" no_link=1]

```

### `no_link`

Default: `0`

Set to `1` for no link `<a>` to term "archive".

### `title_attr`

Default: `0`

Set to `1` to have title attribute on term name (aka tooltip)

### `class`

Class attribute of the output wrapper

Default: 'ch-post-tags'

```php
[ch-post-tags class="super-class" title="Class"]

// output
<aside class="super-class">
<div class="title">Class</div>
<ul class="terms">
	<li class="term"><a href="...">Term Name</a></li>
</ul>
</aside>
```

### `post_id`

Set the post_id.

Default: id of current post/page.

### `wp_typography`

Default: 1

Activates / Deactivates wp_typography filtering.

# Shortcode `[plan-p-sync]`

### ⚠️ Ce shortcode ne doit pas être utilisé sur un contenu accessible publiquement !! ⚠️

Utiliser **uniquement** sur une page **privée** _ou_ **protégée par mot de passe**.

L'appel de ce shortcode déclenche la synchronisation de la taxonomy `plan_c` basée sur les contenu des pages `plan_p`.

```php
[plan-p-sync]
```

---

# Shortcode `[p2p]`

## Description

This shortcode works in combination with plugin posts-2-posts-relationships

https://fr.wordpress.org/plugins/posts-2-posts-relationships/

Originally configured with

- rel_auteur
- rel_source
- any_thing_you_like should work too

## Usage

Requires one of `rel` or `tax` attributes. If both are set, only `rel` is used.

```php
[p2p id="auteur" rel="rel_auteur" title="Autres livres du même auteur" ]

// or

[p2p tax="plan_c" post_type="livre" limit=10 max=30 title="Livres liées à cette section du plan"]
html displayed in footer
[/p2p]

[p2p tax="plan_c" include_children=1 post_type="livre" limit=5 importance="essentiel" orderby="rand" title="Livres essentiels via le plan. (les sous parties du plan inclues)"]
<small><i>Remarque&thinsp;: include_children=1 contrôle l affichage des sous parties.</i></small>
<p>
<button title="Plus de contenu">Plus</button>
</p>
[/p2p]
```

## Attributes

```php

'title' => '',

// Post_IDs Default: curent PostID (self) Ex: 1234 or 1,2,3 or
'id' => '', // number|n,n|auteur|self|citation|livre multiple comma separated
'via' => '', // rel_auteur|rel_source  for the 'magic' replaced keywords in id
		// Default '' = Automatic (rel_auteur pour auteur, et rel_source pour les autres)

// find relations via p2p_relationships
'rel' => '', // rel_auteur|rel_livre or other p2p_relationships
'direction' => 'any', // from_to|to_from|any - p2p_relationships

// or via taxonomy (rel or tax)
'tax' => '', // find related via taxonomy
'tax_operator' => 'AND', // TODO: 'OR' not implementable at this time
'include_children' => 0, // using tax relation, include children

'post_type' => 'any', // 'auteur,livre,citation,post,attachement',
'post_status' => 'publish,inherit',
'post_format' => '', // video,gallery,audio or -video,-link
'limit' => 10, // -1 = unlimited
// attribute max limits the total number to show (ex: multiple auteur could lead to more posts)
// defaults to 3 * attr[limit]
'max' => null,
'orderby' => 'type title',
'order' => 'ASC',
'exclude' => 'self,seen',
'seen' => 'page', // string - seen group name - Set at 0 to disable storing showed post IDs

'importance' => '',  // filter by importance

'class' => 'p2p-shortcode',

'image_size' => 'thumbnail',
'show_thumb' => 1, // display thumbnail when available. Hide = 0
// 0 = never show title
// 1 = display title only when no thumbnail
// 2 = force display title even when thumbnail exists
'show_title' => 1,
'show_excerpt' => 0, // display post excerpt
'show_content' => 0, // display post full content
'show_date' => 0, // display post publish date TODO:

'wp_typography' => 1, // Apply wp_typography filters. 0 = Disabled

```

### `rel`

Default: ''

Post 2 Post relation key. _p2p_rel_key_

Exemples

```php
// will use rel_source Post2Post Relations
[p2p rel="rel_source"]
```

### `tax`

Default: ''

```php
[p2p tax=plan_c post_type=livre importance=essentiel]
<a href="#">+</a> <!-- le contenu ici s'affichera en footer -->
[/p2p]
```

### `title`

Title (wrapped in &lt;h6 /&gt;)

Optional. Default: ''

Hides on empty posts to display.

Exemples

```php
[p2p title="Book Authors"]
```

### `id`

Takes source post_ids related as from_to/to_from/any (see attribute: direction)

Optional.
Default: current post_id

id: number|'self'|'auteur'|'livre'|'citation' (accepts multiple - comma separated)

Exemples

```php
[p2p id="self,auteur" ]

// obtenir les contenus du même auteur
[p2p title="Du même auteur(s)" id="auteur" rel="rel_auteur" post_type="post,attachment,citation,livre" ]


[p2p title="Dans le plan" id="self,livre,citation" tax="plan_c" post_type="post,attachment,citation,livre" ]

```

### `id_magic_via`

Magic des auteur,livre,citation dans les ids en cherchant via 'rel_auteur' ou 'rel_source'

Default: ''

Laissé vide = automatique : il utilisera `rel_auteur` si on est sur une page auteur ou si on demande un `auteur`
et `rel_source` pour les `livre` `citation` etc...

### `post_type`

Optional. Multiple comma separated.
Default: `any`

Filters post type to display.

Exemples

```php
[p2p post_type="attachment"]

[p2p post_type="livre,citation"]

```

### `post_status`

Optional.
Default: 'publish,inherit'

Exemples

```php

[p2p post_status="publish"]

// seems to bug with status=any
// ‘any‘ – retrieves any status except for ‘inherit’, ‘trash’ and ‘auto-draft’.
```

### `post_format`

Optional.
Default ''

Filter posts having format or exclude using -

Exemple

```php
// display videos and galleries
[p2p post_format="video,quote"]


// exclude video and quote
[p2p post_format="-video,-quote"]

// exclude all none Standard formats
[p2p post_format="none"]
```

- aside – Typically styled without a title. Similar to a Facebook note update.
- gallery – A gallery of images. Post will likely contain a gallery shortcode and will have image attachments.
- link – A link to another site. Themes may wish to use the first `<a href="">` tag in the post content as the external link for that post. An alternative approach could be if the post consists only of a URL, then that will be the URL and the title (post_title) will be the name attached to the anchor for it.
- image – A single image. The first `<img />` tag in the post could be considered the image. Alternatively, if the post consists only of a URL, that will be the image URL and the title of the post (post_title) will be the title attribute for the image.
- quote – A quotation. Probably will contain a blockquote holding the quote content. Alternatively, the quote may be just the content, with the source/author being the title.
- status – A short status update, similar to a Twitter status update.
- video – A single video or video playlist. The first `<video />` tag or object/embed in the post content could be considered the video. Alternatively, if the post consists only of a URL, that will be the video URL. May also contain the video as an attachment to the post, if video support is enabled on the blog (like via a plugin).
- audio – An audio file or playlist. Could be used for Podcasting.
- chat – A chat transcript, like so:

```
  John: foo
  Mary: bar
  John: foo 2
```

Source: [post_format](https://wordpress.org/support/article/post-formats/)

### `limit`

Optional. Default: `10`

`-1` = unlimited

Limit number of posts to get per relation (per `attr[id]`)

Exemples

```php
// Tous les livres du même auteur
[p2p limit=-1 id="auteur" post_type="livre"]
```

### `show`

Optional. Default: attr[limit] 0 = unlimited

Sets maximum total items to display.

Exemples

```php
// Maximum 5 livres par auteurs, pas plus de 10 livres en tout.
[p2p id="auteur" post_type="livre" limit=5 show=10]

```

### `orderby`

Optional.
Default `type title`

Check [WP_Query](https://developer.wordpress.org/reference/classes/wp_query/)
for more details.

Exemples

```php
[p2p orderby="none"]
[p2p orderby="ID" order="DESC"]
[p2p orderby="date"]
[p2p orderby="modified"]
[p2p orderby="title"]
[p2p orderby="name"] // by slug
[p2p orderby="rand"]
[p2p orderby="comment_count"]
[p2p orderby="menu_order"] // page order (post_ord)
[p2p orderby="author"]
[p2p orderby="parent"] // Order by post/page parent id.

// idées tordues
[p2p orderby="post__in"]

// not implemented
// meta_value as a ‘meta_key=keyName‘ must also be present in the query
```

### `order`

Optional. ASC|DESC
Default: `ASC`

Exemples

```php
[p2p orderby="post_date" order="DESC"]
```

### `exclude`

Optional. Default: `self,seen`

Can take a list of post IDs to hide

To disable set to -1

Exemples

```php
[p2p exclude="-1"]

[p2p exclude="self,seen,1234"]
```

- `self` Exclude current post
- `seen` Exclude seen posts specified by `attr[seen]` group (see below)

### `seen`

Optional.
Default `page`
Take string

Seen group name. Set to `0` to disable storing seen

#### Exemple

```php
// only consider posts seen in group sidebar
[p2p seen="sidebar"]

[p2p exclude="self" seen="0" title="Don't consider those posts seen"]

// Don't filter out seen, but still store IDs in page group
// default seen="page"
[p2p exclude="self" title="One"]
...
// this allow for other blocks not to repeat any post seen in group One
// Note that exclude="self,seen" seen="page" are the defaults :)
// same as [p2p rel="source" title="Two"]
[p2p rel="source" exclude="self,seen" seen="page" title="Two"]
```

### `class`

Optional. the CSS class of wrapper div.
Default: `p2p-shortcode`,

Exemples

```php
[p2p class="livre-meme-auteur" id="auteur" post_type="livre"]
```

### `direction`

Optional.
Default `any`
Accepts `from_to` | `to_from` | `any`

Exemples

```php
[p2p direction="from_to" rel="source" title="Auteur" post_type="auteur"]

[p2p direction="from_to" rel="source" title="Source" post_type="livre"]

[p2p direction="to_from" rel="source" title="Citations" post_type="citation"]

[p2p direction="to_from" rel="source" title="Cité dans"]

```

### `image_size`

Optional.
Default: `thumbnail`

Common values: `thumbnail` `medium` `large` `full`

Note: is ignored on post type attachment.

Exemples

```php
[p2p image_size="medium"] [p2p image_size="large"]
```

### `importance`

Optional.

Exemples

```php
[p2p importance="essentiel,important"]
```
