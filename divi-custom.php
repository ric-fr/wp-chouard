<?php

/**
 *	Hide the Divi "Project" post type.
 *	Thanks to georgiee (https://gist.github.com/EngageWP/062edef103469b1177bc#gistcomment-1801080) for his improved solution.
 */
add_filter('et_project_posttype_args', 'my_divi_et_project_posttype_args', 10, 1);
function my_divi_et_project_posttype_args($args)
{
	return array_merge($args, array(
		'public'              => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => false
	));
}




/**
 * Enable Divi on custom post types
 */

/* Enable Divi Builder on all post types with an editor box */
function my_divi_add_post_types($post_types)
{
	foreach (get_post_types() as $pt) {
		if (!in_array($pt, $post_types) and post_type_supports($pt, 'editor')) {
			$post_types[] = $pt;
		}
	}
	return $post_types;
}
add_filter('et_builder_post_types', 'my_divi_add_post_types');

/* Add Divi Custom Post Settings box */
function my_divi_add_meta_boxes()
{
	foreach (get_post_types() as $pt) {
		if (post_type_supports($pt, 'editor') and function_exists('et_single_settings_meta_box')) {
			add_meta_box('et_settings_meta_box', __('Divi Custom Post Settings', 'Divi'), 'et_single_settings_meta_box', $pt, 'side', 'high');
		}
	}
}
add_action('add_meta_boxes', 'my_divi_add_meta_boxes');

/* Ensure Divi Builder appears in correct location */
function my_divi_admin_js()
{
	$s = get_current_screen();
	if (
		!empty($s->post_type) &&
		!in_array($s->post_type, ['page', 'post'], true)
	) {
?>
		<script>
			jQuery(function($) {
				$('#et_pb_layout').insertAfter($('#et_pb_main_editor_wrap'));
			});
		</script>
		<style>
			#et_pb_layout {
				margin-top: 20px;
				margin-bottom: 0px
			}
		</style>
<?php
	}
}
add_action('admin_head', 'my_divi_admin_js');

// Ensure that Divi Builder framework is loaded - required for some post types when using Divi Builder plugin
add_filter('et_divi_role_editor_page', 'my_divi_load_builder_on_all_page_types');
function my_divi_load_builder_on_all_page_types($page)
{
	return isset($_GET['page']) ? $_GET['page'] : $page;
}
