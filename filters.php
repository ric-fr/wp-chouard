<?php


function ch_next_page_nav_html()
{
	$html = '';

	$nav = ch_next_page_nav();

	// 	return '<pre>' . print_r($nav, true) . '</pre>';

	if ($nav['next'] || $nav['prev']) {
		$html .= '<nav class="next-page-nav">';

		if (
			$nav['prev']
			&& $prev = get_post($nav['prev'])
		) {
			$html .= "\n";
			$html .= '<a href="' . get_permalink($prev->ID) . '"';
			$html .= ' class="prev"';
			$html .= ' title="' . esc_attr(wp_strip_all_tags($prev->post_title)) . '"';
			$html .= '>Précédent</a>';
		}

		if (
			$nav['next']
			&& $next = get_post($nav['next'])
		) {
			$html .= "\n";
			$html .= '<a href="' . get_permalink($next->ID) . '"';
			$html .= ' class="next"';
			$html .= ' title="' . esc_attr(wp_strip_all_tags($next->post_title)) . '"';
			$html .= '>Suivant</a>';
		}

		//$html .= $nav['pages'];

		$html .= '</nav>';

		return $html;
	}
}

add_filter('the_content', 'ch_single_post_content_filter');
function ch_single_post_content_filter($content)
{
	if (
		is_singular()
		&& is_main_query()
	) {
		$id = get_the_ID();
		$post = get_post($id);

		/**
		 * Nombre de mots dans le post_content
		 */
		$word_count = str_word_count(trim(strip_tags($post->post_content)));


		/**
		 * Plan
		 */

		if ($post->post_type == 'plan_p') {

			$sc = '';

			/**
			 * Plan: next page
			 */

			$next_page_nav = ch_next_page_nav_html();

			// début du content
			$content = '<div class="next-plan-nav-before">'
				. $next_page_nav
				. '</div>'
				. $content;

			// fin du content
			$content .= '<div class="next-plan-nav-before">'
				. $next_page_nav
				. '</div>';


			/**
			 * Vous êtes ici
			 */

			// si le contenu est court, on ne montre pas la navigation dans le contenu
			if ($word_count > 500) {
				$plan_nav = <<<EOD

[post-side-nav title="Vous êtes ici" post_type="plan_p" class="post-content-nav"]
<a href="https://chouard2.adm.article3.net/plan-du-site">Le Plan</a>
[/post-side-nav]

EOD;
			}

			$content .=  do_shortcode($plan_nav);


			$sc = <<<EOD

[p2p title="Livres essentiels" importance="essentiel" tax="plan_c" post_type="livre" limit=-1 class="p2p-shortcode content" image_size=medium]
[p2p title="Livres importants" importance="important" tax="plan_c" post_type="livre" limit=-1 class="p2p-shortcode content" image_size=medium]
[p2p title="Livres utiles" importance="utile" tax="plan_c" post_type="livre" limit=-1 class="p2p-shortcode content" image_size=medium]
[p2p title="Vidéos essentielles" importance="essentiel" tax="plan_c" post_format="video" limit=-1 orderby="date" order="DESC" image_size=medium]
[p2p title="Vidéos importantes" importance="important" tax="plan_c" post_format="video" limit=-1 orderby="date" order="DESC" image_size=medium]
[p2p title="Documents utiles" importance="essentiel,important,utile" tax="plan_c" post_type="attachment" limit=-1 image_size=medium]
[p2p title="Articles importants du Blog" importance="essentiel,important" tax="plan_c" post_type="post" post_format="-video" limit=-1 image_size=medium orderby=date order=DESC include_children=0]
[p2p title="Citations utiles" tax="plan_c" post_type="citation" limit=-1 image_size=medium include_children=0]

EOD;

			$content .=  do_shortcode($sc);
		} // Plan



		/**
		 * Livre
		 */

		if ($post->post_type == 'livre') {
			$sc = <<<EOD
[ch-post-tags title="Importance" tax=importance no_link=1]
[ch-post-tags title="Bibliothèque" tax=thematique]

[ch-post-tags title="Tags" tax=post_tag title_attr=1]
[ch-post-tags title="Sujets" tax=sujet]

[p2p title="Citations du livre" post_type=citation rel=rel_source limit=-1  image_size=medium]
[p2p title="Citations du même auteur" limit=-1 rel=rel_auteur id=auteur post_type=citation image_size=medium]

EOD;

			$content .= '<hr /><section class="post-relations">' . do_shortcode($sc) . '</section>';
		}

		/**
		 * Citation
		 */

		if ($post->post_type == 'citation') {
			$sc = <<<EOD

[p2p title="Citations du même livre" post_type=citation rel=rel_source limit=-1  image_size=medium]

[p2p limit=-1 rel=rel_auteur id=auteur post_type=citation title="Citations du même auteur" image_size=medium]

EOD;

			$content .= do_shortcode($sc);
		}


		if ($post->post_type == 'auteur') {
			$sc = <<<EOD

[p2p title="Citations de l’auteur" rel=rel_auteur post_type=citation image_size=medium orderby="title"]

EOD;

			$content .= do_shortcode($sc);
		}

		/*


		if ($post->post_type != 'post' && false) {

			if (taxonomy_exists('sujet')) {
				$sujets = get_the_terms($id, 'sujet');
				if ($sujets) {
					$array = [];
					foreach ($sujets as $tag) {
						$array[] = '<li><a href="' . get_term_link($tag->term_id) . '"'
							. ' title="' . esc_attr($tag->name) . '"/>' . $tag->name . '</a></li>';
					}

					$content = '<ul class="post-sujets">' . implode(' ', $array) . '</ul>' . "\n" . $content;
				}
			}

			if (taxonomy_exists('post_tag')) {
				$tags = get_the_terms($id, 'post_tag');
				if ($tags) {
					$array = [];

					foreach ($tags as $tag) {
						$array[] = '<li><a href="' . get_term_link($tag->term_id) . '"'
							. ' title="' . esc_attr($tag->name) . '"/>' . $tag->name . '</li></a>';
					}

					$content =  '<ul class="post-tags">' . implode(' ', $array) . '</ul>' . "\n" . $content;
				}
			}

			if (taxonomy_exists('plan_c')) {
				$plan_c = get_the_terms($id, 'plan_c');
				if ($plan_c) {
					$array = [];
					$array[] = '<h6>Dans le Plan</h6>';

					foreach ($plan_c as $tax) {
						$plan_ps = get_posts(
							array(
								'posts_per_page' => -1,
								'post_type' => 'plan_p',
								'tax_query' => array(
									array(
										'taxonomy' => 'plan_c',
										'field' => 'term_id',
										'terms' => $tax->term_id,
									)
								)
							)
						);
						foreach ($plan_ps as $plan_p) {
							$array[] = '<li><a href="' . get_permalink($plan_p->ID) . '"'
								. ' title="' . esc_attr(wp_strip_all_tags($plan_p->post_title)) . '"/>' . $plan_p->post_title . '</a></li>';
						}
					}

					$content = '<ul class="post-plans">' . implode(' ', $array) . '</ul>' . "\n" . $content;
				}
			}

			if (taxonomy_exists('thematique')) {
				$thematiques = get_the_terms($id, 'thematique');
				if ($thematiques) {
					$array = [];
					$array[] = '<h6>Les Thèmes dans le blog</h6>';
					foreach ($thematiques as $tag) {
						$array[] = '<li><a href="' . get_term_link($tag->term_id) . '"'
							. ' title="' . esc_attr($tag->name) . '"/>' . $tag->name . '</a></li>';
					}

					$content .= '<ul class="post-thematiques">' . implode(' ', $array) . '</ul>' . "\n" . $content;
				}
			}
		}

		/*

		// Liste des livres utiles à la page en cours
		if ($post->post_type == 'plan_p') {
			$content .= "\n"
				. '<hr />'
				//				. plan_p_nav($post)
			; // affiche le plan proche de la page en cours

			$terms = wp_get_post_terms($post->ID, 'plan_c');
			if ($terms) {
				$plan_c = $terms[0]; // dans plan_c, je récupère le premier term (le plan_C associé au plan_P en cours)

				// $content .= "\n"
				// 	. '<hr />'
				// 	. '<a href="' . get_term_link($plan_c->term_id, 'plan_c') . '">Contenu à ce sujet</a>';


				$livres = plan_c_livres($plan_c);

				if (!empty(trim($livres))) {
				//	$content .= "\n"
				//		. '<h5>LIVRES IMPORTANTS utiles à la partie en cours :</h5>'
				//		. $livres;
				}
			}
		}

		// */
	}

	return $content;
}


function plan_c_livres($plan_c)
{
	$livres = livres_by_term_shortcode([
		'slug' => $plan_c->slug,
		'taxonomy' => 'plan_c',
		'importance' => 'essentiel,important',
		'image_size' => 'medium'
	]);

	return $livres; // Cette fonction rend une liste de livres : string de html ul li image et titre
}


function plan_p_nav($post)
{
	$r = [];

	// on passe par une fonction intermédiaire pour
	// pouvoir distinguer si on a ou pas des "enfants"
	// ex: n'ajouter un titre que si nécessaire
	//     ou afficher un lien vers page parent


	if ($post->post_parent && $parent = get_post($post->post_parent)) {
		$r[] = '<h4>'
			. '<a href="' . get_permalink($parent->ID) . '">'
			. esc_html(wp_strip_all_tags($parent->post_title))
			. '</a>'
			. '</h4>';
	}

	if (
		$plan_p_nav_content = plan_p_nav_loop($post, [
			'limit_depth' => 2
		])
	) {

		$r[] = '<h5>' . esc_html(wp_strip_all_tags($post->post_title)) . '</h5>';

		$r[] = $plan_p_nav_content;
	}

	return implode("\n", $r);
}

function plan_p_nav_loop($post, $params = ['limit_depth' => 0], $depth = 0)
{

	$r = [];

	$plan_p_children = get_posts([
		'post_type' => 'plan_p',
		'post_status' => 'published',
		'post_parent' => $post->ID,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	]);

	if ($plan_p_children) {
		$r[] = '<ul class="'
			. (
				($depth) ?
				'depth-' . $depth // pour niveaux != 0 on donne classe depth-1 etc...
				: 'plan-p-children' // pour niveau 0 on donne la classe 'plan-p-children'
			)
			. '">';

		foreach ($plan_p_children as $p) {
			// wp_strip_all_tags() => enlever les tags html ex: <mark>
			$r[] = '<li>';
			$r[] = '<a href="' . get_permalink($p->ID) . '">';
			$r[] = esc_html(wp_strip_all_tags($p->post_title));
			$r[] = '</a>';

			if (
				$params['limit_depth'] <= 0 // unlimited
				|| ($params['limit_depth'] > $depth + 1) // or limit not reached
			) {
				$r[] = plan_p_nav_loop($p, $params, $depth + 1);
			}
			$r[] = '</li>';
		}

		$r[] = '</ul>';
	}

	return implode("\n", $r);
}
